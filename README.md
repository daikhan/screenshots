# Screenshots

<picture>
  <source srcset="dark.png" media="(prefers-color-scheme: dark)">
  <img src="light.png">
</picture>

<picture>
  <source srcset="dark-fullscreen.png" media="(prefers-color-scheme: dark)">
  <img src="light-fullscreen.png">
</picture>
